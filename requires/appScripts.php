<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugins/flatpickr.min.js"></script>
<script src="assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="assets/js/plugins/countup.min.js"></script>
<script src="assets/js/plugins/choices.min.js"></script>
<script src="assets/js/plugins/prism.min.js"></script>
<script src="assets/js/plugins/highlight.min.js"></script>
<script src="assets/js/plugins/rellax.min.js"></script>
<script src="assets/js/plugins/tilt.min.js"></script>
<script src="assets/js/plugins/choices.min.js"></script>
<script src="assets/js/plugins/parallax.min.js"></script>
<script src="assets/fontawesome/js/all.min.js" type="text/javascript"></script>
<script src="assets/js/material-kit.min.js?v=3.0.0" type="text/javascript"></script>
<script>
     if (document.getElementById('state1')) {
      const countUp = new CountUp('state1', document.getElementById("state1").getAttribute("countTo"));
      if (!countUp.error) {
        countUp.start();
      } else {
        console.error(countUp.error);
      }
    }
    if (document.getElementById('state2')) {
      const countUp1 = new CountUp('state2', document.getElementById("state2").getAttribute("countTo"));
      if (!countUp1.error) {
        countUp1.start();
      } else {
        console.error(countUp1.error);
      }
    }
    if (document.getElementById('state3')) {
      const countUp2 = new CountUp('state3', document.getElementById("state3").getAttribute("countTo"));
      if (!countUp2.error) {
        countUp2.start();
      } else {
        console.error(countUp2.error);
      };
    }
</script>
</body>
</html>
