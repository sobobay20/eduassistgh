<footer class="footer pt-2 mt-2 p-2">
  <div class="container">
    <div class=" row">
      <div class="col-md-3 col-sm-6 mb-4 ms-auto"> 
        <div>
          <ul class="d-flex flex-row ms-n3 nav">
            <li class="nav-item">
              <a class="nav-link pe-1" href="" target="_blank">
                <i class="fab fa-facebook text-lg opacity-8"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link pe-1" href="" target="_blank">
                <i class="fab fa-twitter text-lg opacity-8"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link pe-1" href="" target="_blank">
                <i class="fab fa-github text-lg opacity-8"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link pe-1" href="" target="_blank">
                <i class="fab fa-youtube text-lg opacity-8"></i>
              </a>
            </li>
          </ul>
        </div>
  
      </div>
  
      <div class="col-md-3 col-sm-6 col-6 mb-4">
        <div>
          <h6 class="text-sm"><a class="nav-link font-weight-bold" href="../index.php">
          Home
          </a></h6>
  
        </div>
      </div>
  
  
      <div class="col-md-3 col-sm-6 col-6 mb-4">
        <div>
          <h6 class="text-sm"><a class="nav-link font-weight-bold" href="../bundle.php">
          Get Bundle
          </a></h6>
  
        </div>
      </div>
  
      <div class="col-md-3 col-sm-6 col-6 mb-4">
        <div>
          <h6 class="text-sm"><a class="nav-link font-weight-bold" href="../about.php">
          About Us
          </a></h6>
  
        </div>
      </div>
  
     
    </div>
  </div>
  
  <div class="row bg-dark">
    <div class="col- md-12">
      <div class="text-center">
        <p class="text-white my-1 text-sm font-weight-normal">
          All rights reserved. Copyright © <script>document.write(new Date().getFullYear())</script> EduAssistantGh
        </p>
      </div>
    </div>
  </div>
</footer>