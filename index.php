<?php require_once "requires/appHead.php";?>
<body class="index-page bg-gray-200">
  
<?php require_once "requires/navbar.php";?>
<header class="header-rounded-images">
  <div class="page-header min-vh-90">
    <img class="position-absolute fixed-top ms-auto w-50 h-100 z-index-0 d-none d-sm-none d-md-block border-radius-section border-top-end-radius-0 border-top-start-radius-0 border-bottom-end-radius-0" src="assets/img/index-bg.jpg" alt="image" loading="lazy">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 d-flex">
          <div class="card card-body blur text-md-start text-center px-sm-5 shadow-lg mt-sm-5 py-sm-5">
            <h3 class="text-dark mb-4">Professional Writing & Proofreading</h3>
            <p class="lead text-dark pe-md-5 me-md-5">
              Get professional assistance with anything essay, research, assignments and the likes from <strong>EduAssistantGh</strong>
            </p>
            <div class="buttons">
              <a href="order.php">
                <button type="button" class="btn bg-secondary mt-4 text-white">Place Your Order</button>
              </a>
              <a href="signup.php">
              <button type="button" class="btn btn-outline-secondary mt-4 ms-2">
                Sign Up
              </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<div class="card card-body blur shadow-blur mx-3 mx-md-4 mt-n6">
  <section id="whyChooseUs" class="py-2">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mx-auto text-center">
          <h2 class="mb-0">Why Choose Us?</h2>
        </div>
      </div>
      <div class="row mt-2">
        <div class="col-lg-3 col-md-8 ms-md-auto">
          <div class="card ">
            <div class="card-body card-height">
                <div class="text-center">
                  <h6 class="mb-0 font-weight-bolder text-dark">Confidentiality</h6>
                </div>       
              <div class="text-center">
                <img src="assets/img/confidentiality.png" alt="" class="pt-4">
              </div>
              <p class="mt-4 text-dark ">Your personal data is safe with us. No third parties will have access to your information.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-8 ms-md-auto">
          <div class="card ">
            <div class="card-body card-height">
                <div class="text-center">
                  <h6 class="mb-0 font-weight-bolder text-dark">Non Plagiarized Papers</h6>
                </div>       
              <div class="text-center">
                <img src="assets/img/no-plag.png" alt="" class="icon">
              </div>
              <p class="mt-4 text-dark "> Principles of good academic work will be employed at this stage. All citations and references would be rightly done.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-8 ms-md-auto">
          <div class="card ">
            <div class="card-body card-height">
                <div class="text-center">
                  <h6 class="mb-0 font-weight-bolder text-dark">Delivery on Time</h6>
                </div>       
              <div class="text-center">
                <img src="assets/img/delivery.png" alt="" class="icon">
              </div>
              <p class="mt-4 text-dark ">Assignments will be submitted on/before the due date.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-8 ms-md-auto">
          <div class="card ">
            <div class="card-body card-height">
                <div class="text-center">
                  <h6 class="mb-0 font-weight-bolder text-dark">Quality Outcome</h6>
                </div>       
              <div class="text-center">
                <img src="assets/img/quality.png" alt="">
              </div>
              <p class="mt-4 text-dark ">We deliver the best results.</p>
            </div>
          </div>
        </div>
      </div>
      <hr class="horizontal dark my-5">
    
    </div>
  </section>
  <section id="howItWorks" class="feature-section pb-5 position-relative bg-dark mx-n3">
    <div class="container">
      <div class="row mt-2">
      <div class="col-md-12 mx-auto text-center">
        <h2 class="mb-0 text-white">How It Works?</h2>
      </div>
    </div>

    <div class="row justify-content-center mt-2">
    <div class="col-lg-4 col-md-8 col-sm-10">
    <div class="card card-height">
    <div class="card-body">
      <div class="single-feature">
        <div class="icon">
        <i class="fas fa-pen mt-3"></i>
        </div>
        <div class="content">
        <h3 class="card-title">Requirements</h3>
        <p>
        Place an order. Tell us about your order requirements, instructions and deadline.
        </p>
        </div>
        </div>
    </div>
    </div>
    </div>
    <div class="col-lg-4 col-md-8 col-sm-10">
    <div class="card card-height">
    <div class="card-body">
      <div class="single-feature">
        <div class="icon">
        <i class="fas fa-eye mt-3"></i>
        </div>
        <div class="content">
        <h3 class="card-title">Review Order</h3>
        <p>
        Based on reviews, we select a professional attendant / writer.
        </p>
        </div>
        </div>
    </div>
    </div>
    </div>
    <div class="col-lg-4 col-md-8 col-sm-10">
    <div class="card card-height">
      <div class="card-body">
        <div class="single-feature">
          <div class="icon">
          <i class="fas fa-wallet mt-3"></i>
          </div>
          <div class="content">
          <h3 class="card-title">Make Payment</h3>
          <p>
          Make payment using the available payment methods.
          </p>
          </div>
          </div>
      </div>
    </div>
    </div>
    </div>
    </div>
  </section>
  <section class="py-5" id="guidelines">
    <div class="container">
      <div class="row mt-2">
        <div class="col-md-12 mx-auto text-center">
          <h2 class="mb-0">Essay Writing Tips and Guidelines</h2>
        </div>
      </div>
      <div class="row mt-5 mb-5">
        <div class="col-lg-6 mt-lg-0 mt-5 ps-lg-0 ps-0">
          <div class="p-3 info-horizontal">
            <div class="icon icon-shape  bg-secondary shadow-secondary text-center">
              <i class="fas fa-check opacity-10 text-white"></i>
            </div>
            <div class="description ps-3">
              <p class="mb-0">Read and understand the topic. </p>
            </div>
          </div>

          <div class="p-3 info-horizontal">
            <div class="icon icon-shape  bg-secondary shadow-secondary text-center">
              <i class="fas fa-check opacity-10 text-white"></i>
            </div>
            <div class="description ps-3">
              <p class="mb-0">Create topic sentences. </p>
            </div>
          </div>
          <div class="p-3 info-horizontal">
            <div class="icon icon-shape  bg-secondary shadow-secondary text-center">
              <i class="fas fa-check opacity-10 text-white"></i>
            </div>
            <div class="description ps-3">
              <p class="mb-0">Plan: Brainstorm and oraganize your thoughts.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mt-lg-0 mt-5 ps-lg-0 ps-0">
          <div class="p-3 info-horizontal">
            <div class="icon icon-shape  bg-secondary shadow-secondary text-center">
              <i class="fas fa-check opacity-10 text-white"></i>
            </div>
            <div class="description ps-3">
              <p class="mb-0">Make good use of the paragraphs. </p>
            </div>
          </div>

          <div class="p-3 info-horizontal">
            <div class="icon icon-shape  bg-secondary shadow-secondary text-center"> 
              <i class="fas fa-check opacity-10 text-white"></i>
            </div>
            <div class="description ps-3">
              <p class="mb-0">Keep a check over the grammar rules. </p>
            </div>
          </div>
          <div class="p-3 info-horizontal">
            <div class="icon icon-shape  bg-secondary shadow-secondary text-center">
              <i class="fas fa-check opacity-10 text-white"></i>
            </div>
            <div class="description ps-3">
              <p class="mb-0">Write down a good concluding paragraph and read through again.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="my-5 pt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6 m-auto">
          <h4>Be the first to get Updates</h4>
          <p class="mb-4">
            Do you want to receive monthly newsletter and  get 
            updates on any of services?
          </p>
          <div class="row">
            <div class="col-8">
              <div class="input-group input-group-outline">
                <label class="form-label">Email Here...</label>
                <input type="text" class="form-control mb-sm-0">
              </div>
            </div>
            <div class="col-4 ps-0">
              <button type="button" class="btn bg-gradient-info mb-0 h-100 position-relative z-index-2">Subscribe</button>
            </div>
          </div>
        </div>
        <div class="col-md-5 ms-auto">
          <div class="position-relative">
            <img class="max-width-50 w-100 position-relative z-index-2" src="assets/img/macbook.png" alt="image">
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php require_once "requires/footer.php";?>
<?php require_once "requires/appScripts.php";?>
