<?php require_once "../requires-services/appHead.php";?>
<body class="about-us bg-gray-200">
<?php require_once "../requires-services/navbar.php";?>
<div class="page-header min-vh-90">
    <img class="position-absolute fixed-top ms-auto w-50 h-100 z-index-0 d-none d-sm-none d-md-block border-radius-section border-top-end-radius-0 border-top-start-radius-0 border-bottom-end-radius-0" src="../assets/img/index-bg.jpg" alt="image" loading="lazy">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 d-flex">
          <div class="card card-body blur text-md-start text-center px-sm-5 shadow-lg mt-sm-5 py-sm-5">
            <h2 class="text-dark mb-4">Proofreading</h2>

            <p class="lead text-dark pe-md-5 me-md-5">
            At this stage, we examine your text carefully
            to find and correct typographical errors and mistakes in grammar, appropriate font size and styles
            and spelling.
            </p>
            <div class="buttons">
              <a href="../order.php">
              <button type="button" class="btn bg-secondary mt-4 text-white">Place an Order</button>
              </a>
              <a href="../signup.php">
              <button type="button" class="btn btn-outline-secondary mt-4 ms-2">Sign Up</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<div class="card card-body shadow-xl mx-3 mx-md-4 mt-n6">
  <section class="py-5"> 
    <div class="container">
      <div class="row">
        <div class="col-md-12 mx-auto">
          <h3 class="mb-0">Essence of Proofreading</h3>
        </div>
        <div class="card-body">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Adds power to our writing</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Improves the quality of the work</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Great time to fix errors</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Boost confidence</span>
                  </div>
              </div>

              <div class="col-md-6 col-sm-12">
                <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Allow us to communicate accurately and effective</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Free Cover letter</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Free CV / Resume</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Free Proofreading</span>
                  </div>
              </div>
            </div>
        </div>
      </div>
    </div> 
  </section>
  <hr>
  <section class="my-5 pt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6 m-auto">
          <h4>Be the first to get Updates</h4>
          <p class="mb-4">
          Do you want to receive monthly newsletter and get updates on any of services?
          </p>
          <div class="row">
            <div class="col-8">
              <div class="input-group input-group-outline">
                <label class="form-label">Email Here...</label>
                <input type="text" class="form-control mb-sm-0">
              </div>
            </div>
            <div class="col-4 ps-0">
              <button type="button" class="btn bg-gradient-info mb-0 h-100 position-relative z-index-2">Subscribe</button>
            </div>
          </div>
        </div>
        <div class="col-md-5 ms-auto">
          <div class="position-relative">
            <img class="max-width-50 w-100 position-relative z-index-2" src="../assets/img/macbook.png" alt="image">
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php require_once "../requires-services/footer.php";?>
<?php require_once "../requires-services/appScripts.php";?>
