<?php require_once "../requires-services/appHead.php";?>
<body class="about-us bg-gray-200">
<?php require_once "../requires-services/navbar.php";?>
<div class="page-header min-vh-90">
    <img class="position-absolute fixed-top ms-auto w-50 h-100 z-index-0 d-none d-sm-none d-md-block border-radius-section border-top-end-radius-0 border-top-start-radius-0 border-bottom-end-radius-0" src="../assets/img/index-bg.jpg" alt="image" loading="lazy">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 d-flex">
          <div class="card card-body blur text-md-start text-center px-sm-5 shadow-lg mt-sm-5 py-sm-5">
            <h2 class="text-dark mb-4">Application Essays</h2>

            <p class="lead text-dark pe-md-5 me-md-5">
              A great opportunity to impress or convince your admission officer. An 
              application essay must stand out.
            </p>
            <div class="buttons">
              <a href="../order.php">
              <button type="button" class="btn bg-secondary mt-4 text-white">Place an Order</button>
              </a>
              <a href="../signup.php">
              <button type="button" class="btn btn-outline-secondary mt-4 ms-2">Sign Up</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<div class="card card-body shadow-xl mx-3 mx-md-4 mt-n6">
  <section class="py-5"> 
    <div class="container">
      <div class="row">
        <div class="col-md-12 mx-auto">
          <h3 class="mb-0">Tips for wrtiing an application essay</h3>
        </div>
        <div class="card-body">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Start early</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Communicate your thought properly
                    </span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Avoid repeating same ideas</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">It should communicate honesty</span>
                  </div>
              </div>

              <div class="col-md-6 col-sm-12">
                <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Your must be proofread</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Must be straightforward and concise</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Support ideas with great examples</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Proofreading</span>
                  </div>
              </div>
            </div>
        </div>
      </div>
    </div> 
  </section>
 
</div>
<?php require_once "../requires-services/footer.php";?>
<?php require_once "../requires-services/appScripts.php";?>
