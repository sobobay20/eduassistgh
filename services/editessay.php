<?php require_once "../requires-services/appHead.php";?>
<body class="about-us bg-gray-200">
<?php require_once "../requires-services/navbar.php";?>
<div class="page-header min-vh-90">
    <img class="position-absolute fixed-top ms-auto w-50 h-100 z-index-0 d-none d-sm-none d-md-block border-radius-section border-top-end-radius-0 border-top-start-radius-0 border-bottom-end-radius-0" src="../assets/img/index-bg.jpg" alt="image" loading="lazy">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 d-flex">
          <div class="card card-body blur text-md-start text-center px-sm-5 shadow-lg mt-sm-5 py-sm-5">
            <h2 class="text-dark mb-4">Essays</h2>

            <p class="lead text-dark pe-md-5 me-md-5">
            A piece of writing that gives the author's own argument.
            </p>
            <div class="buttons">
              <a href="../order.php">
              <button type="button" class="btn bg-secondary mt-4 text-white">Place an Order</button>
              </a>
              <a href="../signup.php">
              <button type="button" class="btn btn-outline-secondary mt-4 ms-2">Sign Up</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<div class="card card-body shadow-xl mx-3 mx-md-4 mt-n6">
  <section class="py-5"> 
    <div class="container">
      <div class="row">
        <div class="col-md-12 mx-auto">
          <h3 class="mb-0">Tips for wrtiing an essay</h3>
        </div>
        <div class="card-body">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Know and understand what the essay is about</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Paragraphing</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Brainstorming</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Organize your thought</span>
                  </div>
              </div>

              <div class="col-md-6 col-sm-12">
                <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Proofreading</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Have a strong introduction</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Free CV / Resume</span>
                  </div>
                  <div class="d-flex pb-3">
                    <i class="material-icons my-auto text-dark">done</i>
                    <span class="text-sm ps-3">Free Proofreading</span>
                  </div>
              </div>
            </div>
        </div>
      </div>
    </div> 
  </section>
 
</div>
<?php require_once "../requires-services/footer.php";?>
<?php require_once "../requires-services/appScripts.php";?>
