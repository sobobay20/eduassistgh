<?php require_once "requires/appHead.php";?>
<body class="index-page bg-gray-200">
<?php require_once "requires/navbar.php";?>
<section class="py-sm-7" id="getBundle">
    <div class="bg-secondary position-relative mx-n3 overflow-hidden">
      <img src="assets/img/shapes/waves-white.svg" alt="pattern-lines" class="position-absolute start-0 top-md-0 w-100 opacity-6">
      <div class="container pb-lg-8 pb-7 pt-5 postion-relative z-index-2 position-relative">
        <div class="row">
          <div class="col-md-7 mx-auto text-center">
            <span class="badge bg-gradient-dark mb-2">Pricing</span>
            <h3 class="text-white">Ready to Bundle?</h3>
            <p class="text-white">Based on the package you buy, you will have direct access to our team <br> of writers who will assist you.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-lg-n8 mt-n6">
      <div class="container">
        <div class="row mt-5">
          <div class="col-lg-4 col-sm-6 mb-lg-0 mb-4">
            <div class="card h-100">
              <div class="card-header text-sm-start text-center pt-4 pb-3 px-4">
                <h5 class="mb-1 text-center">One Month</h5>
                <p class="mb-3 text-sm">Good for a personal or client web/mobile app.</p>
                <h3 class="font-weight-bolder mt-3">
                  GHC200.00
                </h3>
                <a href="" class="btn btn-sm bg-gradient-dark w-100 border-radius-md mt-4 mb-2">Buy now</a>
              </div>
              <hr class="horizontal dark my-0">
              <div class="card-body">
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Proofreading</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Application Essays</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">CV / Resume</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Cover Letter</span>
                </div>
                <!--
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Assignments</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Scholarship Essays</span>
                </div>
                -->
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-sm-6 mb-lg-0 mb-4">
            <div class="card bg-gradient-dark h-100">
              <div class="card-header bg-transparent text-sm-start text-center pt-4 pb-3 px-4 z-index-2">
                <h5 class="mb-1 text-center text-white">Three Months</h5>
                <p class="mb-3 text-sm text-white">Perfect for web/mobile apps or SaaS projects.</p>
                <h3 class="font-weight-bolder mt-3 text-white">
                  GHC550.00
                </h3>
                <a href="" class="btn btn-sm btn-white w-100 border-radius-md mt-4 mb-2">Buy
                  now</a>
              </div>
              <hr class="horizontal light my-0">
              <div class="card-body z-index-2">
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-white">done</i>
                  <span class="text-sm ps-3 text-white">Book review</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-white">done</i>
                  <span class="text-sm ps-3 text-white">Assignments</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-white">done</i>
                  <span class="text-sm ps-3 text-white">Proofreading</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-white">done</i>
                  <span class="text-sm ps-3 text-white">Cover Letter</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-white">done</i>
                  <span class="text-sm ps-3 text-white">CV / Resume</span>
                </div>
                <!--
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-white">done</i>
                  <span class="text-sm ps-3 text-white">Priority support</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-white">done</i>
                  <span class="text-sm ps-3 text-white">Free Updates - 3 month</span>
                </div> -->
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-sm-6 mb-lg-0 mb-4">
            <div class="card h-100">
              <div class="card-header text-sm-start text-center pt-4 pb-3 px-4">
                <h5 class="mb-1 text-center">Six Months</h5>
                <p class="mb-3 text-sm">Deploy large-scale projects which include redistribution.</p>
                <h3 class="font-weight-bolder mt-3">
                  GHC1000.00
                </h3>
                <a href="" class="btn btn-sm bg-gradient-dark w-100 border-radius-md mt-4 mb-2">Buy
                  now</a>
              </div>
              <hr class="horizontal dark my-0">
              <div class="card-body">
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Cover Letter</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Book review</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Assignments</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Free Proofreading</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">3 Reviews</span>
                </div>
                <!--
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Priority support</span>
                </div>
                <div class="d-flex pb-3">
                  <i class="material-icons my-auto text-dark">done</i>
                  <span class="text-sm ps-3">Free Updates - 6 months</span>
                </div> -->
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <p class="text-center mt-5">
              <i class="fa fa-lock" aria-hidden="true"></i> Secured Payment by <b> 2Checkout </b> with:
              <br><br>
              <i class="fa fa-cc-paypal fa-2x" aria-hidden="true"></i> <i class="fa fa-cc-visa fa-2x" aria-hidden="true"></i> <i class="fa fa-cc-mastercard fa-2x" aria-hidden="true"></i> <i class="fa fa-cc-amex fa-2x" aria-hidden="true"></i>
            </p>
            <p class="text-center max-width-500 mx-auto"> <b>Info:</b> Money will be refunded after 24 hours of waiting for a writer. 
            </p>
          </div>
        </div>
      </div>
    </div>
</section>
<?php require_once "requires/footer.php";?>
<?php require_once "requires/appScripts.php";?>