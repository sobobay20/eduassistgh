<?php require_once "requires/appHead.php";?>
<body class="about-us bg-gray-200">
<?php require_once "requires/navbar.php";?>
<header class="header-rounded-images">
  <div class="page-header min-vh-90">
    <img class="position-absolute fixed-top ms-auto w-50 h-100 z-index-0 d-none d-sm-none d-md-block border-radius-section border-top-end-radius-0 border-top-start-radius-0 border-bottom-end-radius-0" src="assets/img/index-bg.jpg" alt="image" loading="lazy">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 d-flex">
          <div class="card card-body blur text-md-start text-center px-sm-5 shadow-lg mt-sm-5 py-sm-5">
            <h2 class="text-dark mb-4">About Us</h2>

            <p class="lead text-dark pe-md-5 me-md-5">
              Find the story of Creative Tim&#39;s most complex design system and
              all the work that make this design available.
            </p>
            <div class="buttons">
              <a href="order.php">
              <button type="button" class="btn bg-secondary mt-4 text-white">Place an Order</button>
              </a>
              <a href="signup.php">
              <button type="button" class="btn btn-outline-secondary mt-4 ms-2">Sign Up</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<div class="card card-body shadow-xl mx-3 mx-md-4 mt-n6">
  <section class="py-5"> 
    <div class="container">
      <div class="row">
        <div class="col-md-12 mx-auto text-center">
          <h3 class="mb-0">Our Beliefs</h3>
        </div>
      </div>
      <div class="row mt-2">
        <div class="col-lg-4">
          <div class="info-horizontal bg-secondary border-radius-xl d-block d-md-flex p-4 card-height-1">
            <i class="material-icons text-white text-3xl">flag</i>
            <div class="ps-0 ps-md-3 mt-3 mt-md-0">
              <h5 class="text-white">Getting Started</h5>
              <p class="text-white">Check the possible ways of working with our product and the necessary files for building your own project.</p>
              <a href="https://www.creative-tim.com/learning-lab/bootstrap/overview/material-kit" class="text-white icon-move-right">
                Let's start
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="info-horizontal bg-secondary border-radius-xl d-block d-md-flex p-4 card-height-1">
            <i class="material-icons text-white text-3xl">flag</i>
            <div class="ps-0 ps-md-3 mt-3 mt-md-0">
              <h5 class="text-white">Getting Started</h5>
              <p class="text-white">Check the possible ways of working with our product and the necessary files for building your own project.</p>
              <a href="https://www.creative-tim.com/learning-lab/bootstrap/overview/material-kit" class="text-white icon-move-right">
                Let's start
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="info-horizontal bg-secondary border-radius-xl d-block d-md-flex p-4 card-height-1">
            <i class="material-icons text-white text-3xl">flag</i>
            <div class="ps-0 ps-md-3 mt-3 mt-md-0">
              <h5 class="text-white">Getting Started</h5>
              <p class="text-white">Check the possible ways of working with our product and the necessary files for building your own project.</p>
              <a href="https://www.creative-tim.com/learning-lab/bootstrap/overview/material-kit" class="text-white icon-move-right">
                Let's start
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div> 
  </section>
  <section class="py-7 bg-secondary"> 
  <div class="container">
      <div class="row align-items-center">
        <div class="col-2"></div>
        <div class="col-md-4 mt-lg-0 mt-4">
          <div class="card">
            <div class="card-body text-center">
              <h5 class="font-weight-normal">
                <a href="javascript:;">For Students</a>
              </h5>
              <p class="mb-0">
                Contact us if you would want EduAssistantGh to write your essays.
              </p>
              <a href="signup.html">
              <button type="button" class="btn bg-gradient-info btn-sm mb-0 mt-3">Place an order</button>
              </a> 
            </div>
          </div>
        </div>
        <div class="col-md-4 mt-lg-0 mt-4">
          <div class="card">
            <div class="card-body text-center">
              <h5 class="font-weight-normal">
                <a href="javascript:;">For Writers</a>
              </h5>
              <p class="mb-0">
                Do you want to work with us? Please send us an email.
              </p>
              <a href="signup.html"> <!--i think there should be a link to eduassistgh's mail-->
              <button type="button" class="btn bg-gradient-info btn-sm mb-0 mt-3">Become a Writer</button>
              </a> 
              
            </div>
          </div>
        </div>
        <div class="col-2"></div>
      </div>
   </div>
  </section>

  <section class="pt-4 pb-6" id="count-stats">
    <div class="container">
      <div class="row justify-content-center text-center mt-2">
        <div class="col-md-3">
          <h1 class="text-gradient text-info" id="state1" countTo="1000">0</h1>
          <h5>Projects</h5>
          <p>Of “high-performing” level are led by a certified project manager</p>
        </div>
        <div class="col-md-3">
          <h1 class="text-gradient text-info"><span id="state2" countTo="60000">0</span>+</h1>
          <h5>Hours</h5>
          <p>That meets quality standards required by our users</p>
        </div>
        <div class="col-md-3">
          <h1 class="text-gradient text-info"><span id="state3" countTo="24">0</span>/7</h1>
          <h5>Support</h5>
          <p>Actively engage team members that finishes on time</p>
        </div>
      </div>
    </div>
  </section>
  <section class="my-5 pt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6 m-auto">
          <h4>Be the first to get Updates</h4>
          <p class="mb-4">
          Do you want to receive monthly newsletter and get updates on any of services?
          </p>
          <div class="row">
            <div class="col-8">
              <div class="input-group input-group-outline">
                <label class="form-label">Email Here...</label>
                <input type="text" class="form-control mb-sm-0">
              </div>
            </div>
            <div class="col-4 ps-0">
              <button type="button" class="btn bg-gradient-info mb-0 h-100 position-relative z-index-2">Subscribe</button>
            </div>
          </div>
        </div>
        <div class="col-md-5 ms-auto">
          <div class="position-relative">
            <img class="max-width-50 w-100 position-relative z-index-2" src="assets/img/macbook.png" alt="image">
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php require_once "requires/footer.php";?>
<?php require_once "requires/appScripts.php";?>
