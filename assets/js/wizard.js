$(document).ready(function(){

    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    
    $(".next").click(function(){
    current_fs = $(this).parent();
    next_fs = $(this).parent().next();
    $(".progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    next_fs.show();
    current_fs.animate({opacity: 0}, {
    step: function(now) {
    opacity = 1 - now;
    
    current_fs.css({
    'display': 'none',
    'position': 'relative'
    });
    next_fs.css({'opacity': opacity});
    },
    duration: 600
    });
});

$(".previous").click(function(){

current_fs = $(this).parent();
previous_fs = $(this).parent().prev();
$(".progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
previous_fs.show();
current_fs.animate({opacity: 0}, {
step: function(now) {
opacity = 1 - now;
current_fs.css({
'display': 'none',
'position': 'relative'
});
previous_fs.css({'opacity': opacity});
},
duration: 600
});
});

$('.radio-group .radio').click(function(){
$(this).parent().find('.radio').removeClass('selected');
$(this).addClass('selected');
});

$(".submit").click(function(){
return false;
})

});

const dt = new DataTransfer();

$(".attachment").on('change', function(e){
	for(var i = 0; i < this.files.length; i++){
		let fileBloc = $('<span/>', {class: 'file-block'}),
			 fileName = $('<span/>', {class: 'name', text: this.files.item(i).name});
		fileBloc.append('<span class="file-delete"><span><i class=\"fas fa-trash-alt\"></span></span>')
			.append(fileName);
		$(".filesList > .files-names").append(fileBloc);
	};
	for (let file of this.files) {
		dt.items.add(file);
	}
	this.files = dt.files;
	$('span.file-delete').click(function(){
		let name = $(this).next('span.name').text();
		$(this).parent().remove();
		for(let i = 0; i < dt.items.length; i++){
			
			if(name === dt.items[i].getAsFile().name){
				
				dt.items.remove(i);
				continue;
			}
		}
		document.getElementsByClassName('attachment').files = dt.files;
	});
});

const dtd = new DataTransfer(); 

$(".fileAttachment").on('change', function(e){
	for(var i = 0; i < this.files.length; i++){
		let fileBloc = $('<span/>', {class: 'file-block'}),
			 fileName = $('<span/>', {class: 'name', text: this.files.item(i).name});
		fileBloc.append('<span class="file-delete"><span><i class=\"fas fa-trash-alt\"></span></span>')
			.append(fileName);
		$(".filesLists > .files-name").append(fileBloc);
	};

	for (let file of this.files) {
		dtd.items.add(file);
	}

	this.files = dtd.files;


	$('span.file-delete').click(function(){
		let name = $(this).next('span.name').text();
	
		$(this).parent().remove();
		for(let i = 0; i < dtd.items.length; i++){
			
			if(name === dtd.items[i].getAsFile().name){
				
				dtd.items.remove(i);
				continue;
			}
		}
		
		document.getElementsByClassName('fileaAttachment').files = dtd.files;
	});
});
