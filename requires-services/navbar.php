<!-- Navbar -->
<div class="container position-sticky z-index-sticky top-0"><div class="row"><div class="col-12">
<nav class="navbar navbar-expand-lg  blur border-radius-xl top-0 z-index-fixed shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
  
  <div class="container-fluid px-0">
    <img src="../assets/img/logo-dark.png" alt="logo" class="logo">
    <button class="navbar-toggler shadow-none ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon mt-2">
        <span class="navbar-toggler-bar bar1"></span>
        <span class="navbar-toggler-bar bar2"></span>
        <span class="navbar-toggler-bar bar3"></span>
      </span>
    </button>
    <div class="collapse navbar-collapse pt-3 pb-2 py-lg-0 w-100" id="navigation">
      <ul class="navbar-nav navbar-nav-hover ms-auto">
        <li class="nav-item ms-lg-auto">
          <a class="nav-link nav-link-icon me-2" href="../index.php">
            <p class="d-inline text-sm z-index-1 font-weight-bold" data-bs-toggle="tooltip" data-bs-placement="bottom" title="">Home</p>
          </a>
        </li>
        <li class="nav-item dropdown dropdown-hover mx-2">
          <a class="nav-link dropdown-toogle ps-2 d-flex cursor-pointer align-items-center" href="#" id="dropdownMenuPages" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services
            <img src="../assets/img/down-arrow-dark.svg" alt="down-arrow" class="arrow ms-auto ms-md-2">
            <!-- <i class="fas fa-down-arrow"></i> -->
          </a>
          <div class="dropdown-menu dropdown-menu-animation dropdown-menu-right" aria-labelledby="dropdownMenuPages">
        
           <div class="dropdown-divider"></div>
           <div class="d-md-flex align-items-start justify-content-start">
            <div>   
             <div class="dropdown-header">
              <h6 class="text-dark font-weight-bolder d-flex align-items-center px-1">
                Writing
              </h6>
             </div>
             <a class="dropdown-item" href="../services/mathwork.php">Math work</a>
             <a class="dropdown-item" href="../services/coverletter.php">Cover letter</a>
             <a class="dropdown-item" href="../services/presentation.php">Presentation</a>
             <a class="dropdown-item" href="../services/resume.php">CV / Resume</a>
             <a class="dropdown-item" href="../services/research.php">Research</a>
             <a class="dropdown-item" href="../services/thesis.php">Thesis/Dissertation</a>
            </div>
            <div>
            <div class="dropdown-header">
              <h6 class="text-dark font-weight-bolder d-flex align-items-center px-1">
                Writing
              </h6>
             </div>
             <a class="dropdown-item" href="../services/assignment.php">Assignment / Coursework</a>
             <a class="dropdown-item" href="../services/proofreading.php">Proofreading</a>
              <a class="dropdown-item" href="../services/bookreview.php">Book review</a>
             <a class="dropdown-item" href="../services/application.php">Application essay</a>
             <a class="dropdown-item" href="../services/scholarship.php">Scholarship essay</a>
            </div>
            <div>
              <div class="dropdown-header">
                <h6 class="text-dark font-weight-bolder d-flex align-items-center px-1">
                  Editing
                </h6>
               </div>
             <a class="dropdown-item" href="../services/editcoverletter.php">Cover letter</a>
             <a class="dropdown-item" href="../services/editresearch.php">Research</a>
             <a class="dropdown-item" href="../services/editthesis.php">Dissertation</a>
             <a class="dropdown-item" href="../services/editessay.php">Essays</a>
            
            </div>
           </div>
          </div>
         </li>
         <!-- <li class="nav-item ms-lg-auto">
          <a class="nav-link nav-link-icon me-2" href="#howItWorks">
            <p class="d-inline text-sm z-index-1 font-weight-bold" data-bs-toggle="tooltip" data-bs-placement="bottom" title="How it works">How It Works</p>
          </a>
        </li> -->
        <li class="nav-item ms-lg-auto">
          <a class="nav-link nav-link-icon me-2" href="../about.php">
            <p class="d-inline text-sm z-index-1 font-weight-bold" data-bs-toggle="tooltip" data-bs-placement="bottom" title="About us">About Us</p>
          </a>
        </li>
  
        <li class="nav-item ms-lg-auto">
          <a class="nav-link nav-link-icon me-2" href="../bundle.php">
            <p class="d-inline text-sm z-index-1 font-weight-bold" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Get bundle">Get Bundle</p>
          </a>
        </li>
      
        <li class="nav-item my-auto ms-3 ms-lg-0">
          <a href="../login.php">
          <button type="button" class="btn btn-sm  bg-secondary text-white  mb-0 me-1 mt-2 mt-md-0">
            Login
          </button>
          </a>
        </li>
      </ul>
    
    </div>
  </div>
</nav>
<!-- End Navbar -->
</div></div></div>
