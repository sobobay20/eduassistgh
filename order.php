<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/WebPage">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
<link rel="icon" type="image/png" href="assets/img/edu.png">
<title>EduAssistantGh</title>
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
<!-- Nucleo Icons -->
<link href="assets/css/nucleo-icons.css" rel="stylesheet" />
<link href="assets/css/nucleo-svg.css" rel="stylesheet" />

<!-- Font Awesome Icons -->
<link href="assets/fontawesome/css/all.min.css" rel="stylesheet" />
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<!-- Material Icons -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/css/fileupload/style.css">
<link rel="stylesheet" href="assets/css/fileupload/style.scss">
<link id="pagestyle" href="assets/css/material-kit.css?v=3.0.0" rel="stylesheet" />
<link href="assets/css/style.css" rel="stylesheet" />
</head>
<body class="index-page bg-gray-200">
 <!-- Navbar Dark -->

<nav
class="navbar navbar-expand-lg navbar-dark bg-dark z-index-3 py-3">
<div class="container">
  <img src="assets/img/logo-light.png" alt="logo" class="logo">
</div>
</nav>
<!-- End Navbar -->
  <section class="pb-5 position-relative bg-gradient-light mx-n3">
    <div class="p-5 mt-n5">
      <div class="row mt-0">
        <div class="col-md-12">
          <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
            <div class="row">
              <div class="col-md-12">
             <!-- <div class="container"> -->
             <form id="msform" >
              <div class="nav-wrapper position-relative end-0 container">
                  <ul class="nav nav-pills nav-fill p-1" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1 active" data-bs-toggle="tab" href="#writingPane" role="tab" aria-controls="dashboard" aria-selected="false">
                       Writing
                        </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#editingPane" role="tab" aria-controls="dashboard" aria-selected="false">
                     Editing
                      </a>
                    </li>
                  </ul>
              </div>
              <div class="tab-content  border-radius-lg mt-5">
                  <div class="tab-pane fade position-relative border-radius-lg active show" id="writingPane" role="tabpanel" aria-labelledby="dashboard-tabs-simple" loading="lazy">
                        <ul class="progressbar">
                            <li class="active" id="require"><strong>Requirements</strong></li>
                            <li id="review"><strong>Review</strong></li>
                            <li id="payment"><strong>Payment</strong></li>
                            <li id="confirm"><strong>Finish</strong></li>
                        </ul>
                        <form enctype='multipart/form-data' method="post" action="">
                        <fieldset>
                            <div class="form-card mb-5">
                                <div class="row">
                                   <!-- Column 1  -->
                                  <div class="col-md-4">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <label class="form-label">Number of Pages</label>
                                        <div class="input-group input-group-dynamic">
                                          <input type="number" class="form-control" >
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6 col-sm-12">
                                      <label class="float-left" >Deadline 
                                        <!-- <span class="text-info" id="demo"></span><span class="text-info"> day(s) left</span> -->
                                      </label>
                                      <div class="input-group input-group-dynamic flatpickr">
                                        <input class="form-control datepicker" type="text" data-input >
                                      </div>
                                      </div>
                                      <div class="col-md-6 col-sm-12">
                                      <label class="float-left" >Time</label>
                                      <div class="input-group input-group-dynamic">
                                        <input type="time" class="form-control"> 
                                      </div>
                                    
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <label class="float-left" >Subject</label>
                                        <div class="input-group input-group-dynamic">  
                                            <select class="form-control p-2">
                                                <option value="88" selected></option>
                                                <option value="35">English</option>
                                                <option value="63">Nursing</option>
                                                <option value="37">History</option>
                                                <option value="20">Accounting</option>
                                                <option value="69">Anthropology</option>
                                                <option value="2">Architecture</option>
                                                <option value="90">Art, Theatre and Film</option>
                                                <option value="10">Biology</option>
                                                <option value="92">Business and Entrepreneurship</option>
                                                <option value="12">Chemistry</option>
                                                <option value="15">Communication Strategies</option>
                                                <option value="83">Computer Science</option>
                                                <option value="48">Criminology</option>
                                                <option value="19">Economics</option>
                                                <option value="29">Education</option>
                                                <option value="34">Engineering</option>
                                                <option value="71">Environmental Issues</option>
                                                <option value="36">Ethics</option>
                                                <option value="24">Finance</option>
                                                <option value="72">Geography</option>
                                                <option value="62">Healthcare</option>
                                                <option value="37">History</option>
                                                <option value="91">International and Public Relations</option>
                                                <option value="93">Law and Legal Issues</option>
                                                <option value="50">Linguistics</option>
                                                <option value="51">Literature</option>
                                                <option value="57">Management</option>
                                                <option value="58">Marketing</option>
                                                <option value="59">Mathematics</option>
                                                <option value="7">Music</option>
                                                <option value="63">Nursing</option>
                                                <option value="64">Nutrition</option>
                                            </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row mt-1">
                                      <div class="col-md-12">
                                          <label class="form-label">Topic/Main Idea</label>
                                        <div class="input-group input-group-dynamic">
                                          <input type="text" class="form-control" >
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Column 2  -->
                                  <div class="col-md-4">
                                    <label class="float-left" >Detailed Instructions</label>
                                    <div class="input-group input-group-dynamic"> 
                                      <textarea class="form-control " name="" id="" cols="5" rows="8"></textarea>  
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12">
                                      <div class="upload-file">    
                                          <div class="upload-wrapper">
                                              <label>
                                                  <input type="file" name="writingFile" class="fileAttachment" multiple accept=".pdf,.doc">
                                                  <p class="text-info">Attach Files</p>
                                              </label>
                                          </div>
                                        </div>
                                      </div> 
                                      <div class="col-md-12">
                                      <!-- <p class="mb-0">Uploaded Files</p> -->
                                        <p class="files-area">
                                          <span class="filesLists">
                                            <span class="files-name"></span>
                                          </span>
                                        </p>
                                      </div>
                                    
                                    </div>
                                  </div>
                                    <!-- Column 3` -->
                                  <div class="col-md-4">
                                      <div class="row">
                                        <label class="float-left font-weight-bolder" >Writer Type Information</label>
                                        <div class="col-md-12 col-sm-12 ms-auto">
                                        <div class="nav-wrapper position-relative end-0">
                                          <ul class="nav nav-pills nav-fill p-1" is="v-pills-tab" role="tablist">
                                              <li class="nav-item">
                                                <a class="nav-link active" id="v-pills-5-tab" data-bs-toggle="pill" href="#v-pills-5" role="tab" aria-controls="v-pills-home" aria-selected="true">5 Star</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" id="v-pills-4-tab" data-bs-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-profile" aria-selected="false">4 Star</a>
                                              </li>
                                  
                                              <li class="nav-item">
                                                <a class="nav-link" id="v-pills-3-tab" data-bs-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-messages" aria-selected="false">3 Star</a>
                                              </li>
                                            </ul>
                                          </div>
                                        
                                          <div class="tab-content" id="v-pills-tabContent">
                                            <div class="tab-pane fade show active" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5-tab">
                                                <p class="mt-4">
                                                A <strong>5</strong> Star writer is a professional writer who has been reviewed by over 1000 students as the best writer.
                                                <strong>Hiring a 5 star rated writer will cost more than a 4 Star writer</strong>
                                                </p>
                                                <div class="rating mt-1">
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4-tab">
                                                <p class="mt-4">
                                                A <strong>4</strong> Star writer is a professional writer who has been reviewed by over 500 but less than 1000 students as the best writer.
                                                  <strong>Hiring a 4 star writer will cost more than 3 a star writer</strong>
                                                </p>
                                                <div class="rating mt-1">
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="far fa-star"></i>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
                                              <p class="mt-4">
                                              A <strong>3</strong> Star writer is a professional writer who has been reviewed by less than 500 students as the best writer.
                                                <strong>Hiring a 3 star writer will cost less than a 4 star rated writer</strong>
                                              </p>
                                              <div class="rating mt-1">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                              </div>
                                            </div>
                                            <!-- <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                              If you think you can learn a lot from reading books, try writing one.
                                            </div> -->
                                          </div>
                                          </div>
                                      </div>
                          
                                      <label class="float-left mt-3" >Writer Type</label>
                                      <div class="input-group input-group-dynamic">
                                          <select class="form-control p-2">
                                              <option selected>Choose Writer Type</option>
                                              <option>5 Star</option>
                                              <option>4 Star</option>
                                              <option>3 Star</option>
                                          </select>
                                      </div>
                                  </div>
                                </div>   
                            </div> 
                            <input type="button" name="next" class="next action-button" value="Next Step" />
                        </fieldset>
            
                        <fieldset>
                          <div class="row">
                            <div class="col-md-12">
                            <div class="form-card">
                              <div class="row">
                                <!-- COlumn 1  -->
                                <div class="col-md-4">
                                   <div class="row">
                                      <div class="col-md-12">
                                        <label class="form-label font-weight-bold">Topic</label>
                                        <p class="">Some Kind of Topic</p>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <label class="form-label font-weight-bold">Subject</label>
                                        <p class="">Selected Subject</p>
                                      </div>
                                    </div>
                                </div>
                                <!-- Column 2  -->
                                <div class="col-md-4">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <label class="form-label font-weight-bold">Pages</label>
                                        <p class="">6 Pages/5000 words</p>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <label class="form-label font-weight-bold">Deadline</label>
                                        <p class="">December, 30 - 1:40pm</p>
                                      </div>
                                    </div>
                                </div>
                                <!-- Column 3  -->
                                <div class="col-md-4">  
                                  <div class="d-flex p-2">
                                    <img alt="Image" src="assets/img/blank.png" class="avatar shadow">
                                    <div class="ms-3">
                                      <h6 class="mb-0">Anonymous Writer</h6>
                                      <p class="text-muted text-xs mb-2">5 Star Writer</p>
                                      <span class="text-muted text-sm col-11 p-0 d-block">99% success rate</span>
                                      <div class="rating mt-1">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              </div> 
                            </div>
                          </div>
                          <div class="row mt-2">
                             <div class="col-md-7">
                             <div class="form-card">
                               <h4 class="mt-n1">Additional Services</h4>
                               <div class="row">
                                 <div class="col-md-12">
                                    
                                 </div>
                               </div>
                                </div> 
                              </div>
                              <div class="col-md-5">
                                <div class="form-card">
                                <h4 class="mt-n1">Order Summary</h4>
                               <div class="row">
                                 <div class="col-md-12">

                                 </div>
                               </div>
                                </div> 
                              </div>
                          </div>
                           
                            <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                        </fieldset>
                        </form>
                        <fieldset>
                            <div class="container">
                                <h2 class="fs-title">Payment Information</h2>
                                <div class="container">
                                <img src="assets/img/paystack-gh.png" style="width: 400px; height: 150px;" alt="">
                                </div><br><br>
                                <label class="pay">Card Holder Name*</label> <input type="text" name="holdername" placeholder="" />
                                <div class="row">
                                    <div class="col-9"> <label class="pay">Card Number*</label> <input type="text" name="cardno" placeholder="" /> </div>
                                    <div class="col-3"> <label class="pay">CVC*</label> <input type="password" name="cvcpwd" placeholder="***" /> </div>
                                </div>
                                <div class="row">
                                    <div class="col-3"> <label class="pay">Expiry Date*</label> </div>
                                    <div class="col-9"> 
                                      <select class="list-dt" id="month" name="expmonth">
                                            <option selected>Month</option>
                                            <option>January</option>
                                            <option>February</option>
                                            <option>March</option>
                                            <option>April</option>
                                            <option>May</option>
                                            <option>June</option>
                                            <option>July</option>
                                            <option>August</option>
                                            <option>September</option>
                                            <option>October</option>
                                            <option>November</option>
                                            <option>December</option>
                                        </select> <select class="list-dt" id="year" name="expyear">
                                            <option selected>Year</option>
                                        </select> </div>
                                </div>
                            </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Confirm" />
                        </fieldset>
                        <fieldset>
                            <div class="container">
                                <h2 class="fs-title text-center">Success !</h2> <br><br>
                                <div class="row justify-content-center">
                                    <div class="col-3"> <img src="assets/img/success.png" class="fit-image"> </div>
                                </div> <br><br>
                                <div class="row justify-content-center">
                                    <div class="col-7 text-center">
                                        <h5>You Have Successfully Signed Up</h5>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                  </div>
                  <div class="tab-pane fade position-relative border-radius-lg " id="editingPane" role="tabpanel" aria-labelledby="dashboard-tabs" loading="lazy">      
                       <ul class="progressbar">
                            <li class="active" id="require"><strong>Requirements</strong></li>
                            <li id="review"><strong>Review</strong></li>
                            <li id="payment"><strong>Payment</strong></li>
                            <li id="confirm"><strong>Finish</strong></li>
                        </ul> 
                        <fieldset>
                            <div class="form-card mb-5">
                                <div class="row">
                                   <!-- Column 1  -->
                                  <div class="col-md-4">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <label class="form-label">Number of Pages</label>
                                        <div class="input-group input-group-dynamic">
                                          <input type="number" class="form-control" >
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6 col-sm-12">
                                      <label class="float-left" >Deadline 
                                        <!-- <span class="text-info" id="demo"></span><span class="text-info"> day(s) left</span> -->
                                      </label>
                                      <div class="input-group input-group-dynamic flatpickr">
                                        <input class="form-control datepicker" type="text" data-input >
                                      </div>
                                      </div>
                                      <div class="col-md-6 col-sm-12">
                                      <label class="float-left" >Time</label>
                                      <div class="input-group input-group-dynamic">
                                        <input type="time" class="form-control"> 
                                      </div>
                                    
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <label class="float-left" >Subject</label>
                                        <div class="input-group input-group-dynamic">  
                                            <select class="form-control p-2">
                                                <option value="88" selected></option>
                                                <option value="35">English</option>
                                                <option value="63">Nursing</option>
                                                <option value="37">History</option>
                                                <option value="20">Accounting</option>
                                                <option value="69">Anthropology</option>
                                                <option value="2">Architecture</option>
                                                <option value="90">Art, Theatre and Film</option>
                                                <option value="10">Biology</option>
                                                <option value="92">Business and Entrepreneurship</option>
                                                <option value="12">Chemistry</option>
                                                <option value="15">Communication Strategies</option>
                                                <option value="83">Computer Science</option>
                                                <option value="48">Criminology</option>
                                                <option value="19">Economics</option>
                                                <option value="29">Education</option>
                                                <option value="34">Engineering</option>
                                                <option value="71">Environmental Issues</option>
                                                <option value="36">Ethics</option>
                                                <option value="24">Finance</option>
                                                <option value="72">Geography</option>
                                                <option value="62">Healthcare</option>
                                                <option value="37">History</option>
                                                <option value="91">International and Public Relations</option>
                                                <option value="93">Law and Legal Issues</option>
                                                <option value="50">Linguistics</option>
                                                <option value="51">Literature</option>
                                                <option value="57">Management</option>
                                                <option value="58">Marketing</option>
                                                <option value="59">Mathematics</option>
                                                <option value="7">Music</option>
                                                <option value="63">Nursing</option>
                                                <option value="64">Nutrition</option>
                                            </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row mt-1">
                                      <div class="col-md-12">
                                      <label class="float-left mt-3" >Formatting Style</label>
                                      <div class="input-group input-group-dynamic">
                                          <select class="form-control p-2">
                                              <option selected></option>
                                              <option>Format 1</option>
                                              <option>Format 2</option>
                                              <option>Format 3</option>
                                              <option>Format 4</option>
                                              <option>Format 5</option>
                                          </select>
                                      </div>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Column 2  -->
                                  <div class="col-md-4">
                                    <label class="float-left" >Detailed Instructions</label>
                                    <div class="input-group input-group-dynamic"> 
                                      <textarea class="form-control " name="" id="" cols="5" rows="8"></textarea>  
                                    </div>
                                    <div class="row">
                                    <label class="float-left" >Files for editing</label>
                                      <div class="col-md-12">
                                      <div class="upload-file">    
                                          <div class="upload-wrapper">
                                              <label>
                                                  <input type="file" name="editingFile" class="attachment" multiple accept=".pdf,.doc">
                                                  <p class="text-info">Attach Files</p>
                                              </label>
                                          </div>
                                        </div>
                                      </div> 
                                      <div class="col-md-12">
                                      <!-- <p class="mb-0">Uploaded Files</p> -->
                                        <p class="files-area">
                                          <span class="filesList">
                                            <span class="files-names"></span>
                                          </span>
                                        </p>
                                      </div>
                                    
                                    </div>
                                   
                                  </div>
                                    <!-- Column 3` -->
                                  <div class="col-md-4">
                                      <div class="row">
                                        <label class="float-left font-weight-bolder" >Editor Type Information</label>
                                        <div class="col-md-12 col-sm-12 ms-auto">
                                        <div class="nav-wrapper position-relative end-0">
                                          <ul class="nav nav-pills nav-fill p-1" is="v-pills-edit-tab" role="tablist">
                                              <li class="nav-item">
                                                <a class="nav-link active" id="v-pills-edit-1-tab" data-bs-toggle="pill" href="#v-pills-edit-5" role="tab" aria-controls="v-pills-5star" aria-selected="true">5 Star</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" id="v-pills-edit-2-tab" data-bs-toggle="pill" href="#v-pills-edit-4" role="tab" aria-controls="v-pills-4star" aria-selected="false">4 Star</a>
                                              </li>
                                  
                                              <li class="nav-item">
                                                <a class="nav-link" id="v-pills-edit-3-tab" data-bs-toggle="pill" href="#v-pills-edit-3" role="tab" aria-controls="v-pills-3star" aria-selected="false">3 Star</a>
                                              </li>
                                            </ul>
                                          </div>
                                        
                                          <div class="tab-content" id="v-pills-tabContent">
                                            <div class="tab-pane fade show active" id="v-pills-edit-5" role="tabpanel" aria-labelledby="v-pills-edit-1-tab">
                                                <p class="mt-4">
                                                A <strong>5</strong> Star editor is a professional editor who has been reviewed by over 1000 students as the best editor.
                                                <strong>Hiring a 5 star rated editor will cost more than a 4 Star editor</strong>
                                                </p>
                                                <div class="rating mt-1">
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-edit-4" role="tabpanel" aria-labelledby="v-pills-edit-2-tab">
                                                <p class="mt-4">
                                                A <strong>4</strong> Star editor is a professional editor who has been reviewed by over 500 but less than 1000 students as the best editor.
                                                  <strong>Hiring a 4 star editor will cost more than 3 a star editor</strong>
                                                </p>
                                                <div class="rating mt-1">
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="fas fa-star"></i>
                                                  <i class="far fa-star"></i>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-edit-3" role="tabpanel" aria-labelledby="v-pills-edit-3-tab">
                                              <p class="mt-4">
                                              A <strong>3</strong> Star editor is a professional editor who has been reviewed by less than 500 students as the best editor.
                                                <strong>Hiring a 3 star editor will cost less than a 4 star rated editor</strong>
                                              </p>
                                              <div class="rating mt-1">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                              </div>
                                            </div>
                                            <!-- <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                              If you think you can learn a lot from reading books, try writing one.
                                            </div> -->
                                          </div>
                                          </div>
                                      </div>
                          
                                      <label class="float-left mt-3" >Editor Type</label>
                                      <div class="input-group input-group-dynamic">
                                          <select class="form-control p-2">
                                              <option selected>Choose Editor Type</option>
                                              <option>5 Star</option>
                                              <option>4 Star</option>
                                              <option>3 Star</option>
                                          </select>
                                      </div>
                                  </div>
                                </div>   
                            </div> 
                            <input type="button" name="next" class="next action-button" value="Next Step" />
                        </fieldset>
                        <fieldset>
                          <div class="row">
                            <div class="col-md-12">
                            <div class="form-card">
                              <div class="row">
                                <!-- COlumn 1  -->
                                <div class="col-md-4">
                                   <div class="row">
                                      <div class="col-md-12">
                                        <label class="form-label font-weight-bold">Topic</label>
                                        <p class="">Some Kind of Topic</p>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <label class="form-label font-weight-bold">Subject</label>
                                        <p class="">Selected Subject</p>
                                      </div>
                                    </div>
                                </div>
                                <!-- Column 2  -->
                                <div class="col-md-4">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <label class="form-label font-weight-bold">Pages</label>
                                        <p class="">6 Pages/5000 words</p>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <label class="form-label font-weight-bold">Deadline</label>
                                        <p class="">December, 30 - 1:40pm</p>
                                      </div>
                                    </div>
                                </div>
                                <!-- Column 3  -->
                                <div class="col-md-4">  
                                  <div class="d-flex p-2">
                                    <img alt="Image" src="assets/img/blank.png" class="avatar shadow">
                                    <div class="ms-3">
                                      <h6 class="mb-0">Anonymous Editor</h6>
                                      <p class="text-muted text-xs mb-2">5 Star Editor</p>
                                      <span class="text-muted text-sm col-11 p-0 d-block">99% success rate</span>
                                      <div class="rating mt-1">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              </div> 
                            </div>
                          </div>
                          <div class="row mt-2">
                             <div class="col-md-7">
                             <div class="form-card">
                               <h4 class="mt-n1">Additional Services</h4>
                               <div class="row">
                                 <div class="col-md-12">

                                 </div>
                               </div>
                                </div> 
                              </div>
                              <div class="col-md-5">
                                <div class="form-card">
                                <h4 class="mt-n1">Order Summary</h4>
                               <div class="row">
                                 <div class="col-md-12">

                                 </div>
                               </div>
                                </div> 
                              </div>
                          </div>
                            <!-- <div class="form-card">
                                
                            </div>  -->
                            <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                        </fieldset>
                        <fieldset>
                            <div class="container">
                                <h2 class="fs-title">Payment Information</h2>
                                <div class="container">
                                <img src="assets/img/paystack-gh.png" style="width: 400px; height: 150px;" alt="">
                                </div><br><br>
                                
                                <label class="pay">Card Holder Name*</label> <input type="text" name="holdername" placeholder="" />
                                <div class="row">
                                    <div class="col-9"> <label class="pay">Card Number*</label> <input type="text" name="cardno" placeholder="" /> </div>
                                    <div class="col-3"> <label class="pay">CVC*</label> <input type="password" name="cvcpwd" placeholder="***" /> </div>
                                </div>
                                <div class="row">
                                    <div class="col-3"> <label class="pay">Expiry Date*</label> </div>
                                    <div class="col-9"> <select class="list-dt" id="month" name="expmonth">
                                            <option selected>Month</option>
                                            <option>January</option>
                                            <option>February</option>
                                            <option>March</option>
                                            <option>April</option>
                                            <option>May</option>
                                            <option>June</option>
                                            <option>July</option>
                                            <option>August</option>
                                            <option>September</option>
                                            <option>October</option>
                                            <option>November</option>
                                            <option>December</option>
                                        </select> <select class="list-dt" id="year" name="expyear">
                                            <option selected>Year</option>
                                        </select> </div>
                                </div>
                            </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Confirm" />
                        </fieldset>
                        <fieldset>
                            <div class="container">
                                <h2 class="fs-title text-center">Success !</h2> <br><br>
                                <div class="row justify-content-center">
                                    <div class="col-3"> <img src="assets/img/success.png" class="fit-image"> </div>
                                </div> <br><br>
                                <div class="row justify-content-center">
                                    <div class="col-7 text-center">
                                        <h5>You Have Successfully Signed Up</h5>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    
                   
                  </div>
              </div>
             </form>
             <!-- </div> -->
              </div>
            </div>
            
          </div>
      </div>
  </div>
    </div>
  </section>
 
  <footer class="footer pt-2 mt-2 p-2">
    <div class="container">
      <div class=" row">
        <div class="col-md-2 col-sm-6 mb-4 ms-auto"> 
          <div>
            <ul class="d-flex flex-row ms-n3 nav">
              <li class="nav-item">
                <a class="nav-link pe-1" href="https://www.facebook.com/CreativeTim" target="_blank">
                  <i class="fab fa-facebook text-lg opacity-8"></i>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link pe-1" href="https://twitter.com/creativetim" target="_blank">
                  <i class="fab fa-twitter text-lg opacity-8"></i>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link pe-1" href="https://github.com/creativetimofficial" target="_blank">
                  <i class="fab fa-github text-lg opacity-8"></i>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link pe-1" href="https://www.youtube.com/channel/UCVyTG4sCw-rOvB9oHkzZD1w" target="_blank">
                  <i class="fab fa-youtube text-lg opacity-8"></i>
                </a>
              </li>
            </ul>
          </div>
    
        </div>
    
        <div class="col-md-2 col-sm-6 col-6 mb-4">
          <div>
            <h6 class="text-sm"><a class="nav-link font-weight-bold" href="index.php">
            Home
            </a></h6>
    
          </div>
        </div>
    
        <div class="col-md-2 col-sm-6 col-6 mb-4">
          <div>
            <h6 class="text-sm"><a class="nav-link font-weight-bold" href="index.php#whyChooseUs">
            Why Choose Us?
            </a></h6>
    
          </div>
        </div>
    
        <div class="col-md-2 col-sm-6 col-6 mb-4">
          <div>
            <h6 class="text-sm"><a class="nav-link font-weight-bold" href="bundle.php">
            Get Bundle
            </a></h6>
    
          </div>
        </div>
    
        <div class="col-md-2 col-sm-6 col-6 mb-4">
          <div>
            <h6 class="text-sm"><a class="nav-link font-weight-bold" href="index.php#guidelines">
             Writing Guidelines
            </a></h6>
    
          </div>
        </div>
    
        <div class="col-md-2 col-sm-6 col-6 mb-4">
          <div>
            <h6 class="text-sm"><a class="nav-link font-weight-bold" href="about.php">
            About Us
            </a></h6>
    
          </div>
        </div>
    
       
      </div>
    </div>
    
    <div class="row bg-dark">
      <div class="col- md-12">
        <div class="text-center">
          <p class="text-white my-1 text-sm font-weight-normal">
            All rights reserved. Copyright © <script>document.write(new Date().getFullYear())</script>
          </p>
        </div>
      </div>
    </div>
  </footer>
<!--   Core JS Files   -->
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/popper.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/wizard.js" type="text/javascript"></script>
<script src="assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="assets/js/plugins/flatpickr.min.js"></script>
<script src="assets/js/plugins/countup.min.js"></script>
<script src="assets/js/plugins/prism.min.js"></script>
<script src="assets/js/plugins/highlight.min.js"></script>
<script src="assets/js/plugins/rellax.min.js"></script>
<script src="assets/js/plugins/tilt.min.js"></script>
<script src="assets/js/plugins/choices.min.js"></script>
<script src="assets/js/plugins/parallax.min.js"></script>
<script src="assets/fontawesome/js/all.min.js" type="text/javascript"></script>
<script src="assets/js/material-kit.min.js?v=3.0.0" type="text/javascript"></script>
<script>
  if (document.querySelector(".datepicker")) {
     flatpickr(".datepicker", {
         
     });
  }
</script>
</body>
</html>
